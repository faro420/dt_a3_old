--@authors: Mir Farshid Baha,2141801, mirfarshid.baha@haw-hamburg.de
--          Mehmet Cakir, 2195657, mehmet.cakir@haw-hamburg.de
architecture aril of counter is
    signal err_cs : std_logic := '0';
    signal err_ns : std_logic;
    signal cnt_cs:  std_logic_vector(11 downto 0) := (others => '0');
    signal cnt_ns:  std_logic_vector(11 downto 0);
    signal up_cs: std_logic := '0';		
    signal down_cs: std_logic := '0';

begin
    did <= "010";
    
    calculate:
    process(up_cs, down_cs) is
    --variables belong here
        variable cnt_v : std_logic_vector(11 downto 0);
        variable up_v : std_logic;
        variable down_v : std_logic;
        variable addBit : std_logic_vector(11 downto 0);
        variable subBit : std_logic_vector(11 downto 0);
        variable carryBit_v : std_logic;
	    variable err_v: std_logic;
    begin
		cnt_v := cnt_cs;
		up_v := up_cs;
		down_v := down_cs;
		carryBit_v := '0';
		subBit := (others => '1');
		addBit := (others => '0');
		addBit(0) := '1';
		err_v := err_cs;
    --procedure belongs here

		if (up_v = '1') and (down_v = '0') then
			for I in cnt_v'low to cnt_v'high loop
				cnt_v(I) := cnt_v(I) xor subBit(I) xor carryBit_v;
				carryBit_v := (cnt_v(I) and subBit(I)) or (cnt_v(I) and carryBit_v) or (subBit(I) and carryBit_v);
			end loop;
			if carryBit_v = '0' then
				err_v:= '1';
			else
				err_v:= err_cs;
			end if;
		elsif (up_v = '0') and (down_v = '1') then
			for I in cnt_v'low to cnt_v'high loop
				cnt_v(I) := cnt_v(I) xor addBit(I) xor carryBit_v;
				carryBit_v := (cnt_v(I) and addBit(I)) or (cnt_v(I) and carryBit_v) or (addBit(I) and carryBit_v);
			end loop;
			if carryBit_v = '1' then
				err_v:= '1';
			else
				err_v:= err_cs;
			end if;
		else        
				err_v:= err_cs;
		end if;
		err_ns <= err_v;
		cnt_ns <= cnt_v;
    end process calculate;
    
    regCheck:
    process ( clk ) is
    begin
    if clk = '1' and clk'event then
        if nres = '0' then
            err_cs <= '0';
            cnt_cs <= (others => '0');
            up_cs <= '0';
            down_cs <= '0';
        else
		    if nLd = '0' then
		       cnt_cs <= valLd;
            else
               cnt_cs <= cnt_ns;
            end if;			
            err_cs <= err_ns;
            up_cs <= up;
            down_cs <= down;
        end if;    
    end if;
    end process regCheck;
    
    err <= err_cs;
    cnt <= cnt_cs;
end architecture aril;