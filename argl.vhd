--@authors: Mir Farshid Baha,2141801, mirfarshid.baha@haw-hamburg.de
--          Mehmet Cakir, 2195657, mehmet.cakir@haw-hamburg.de
architecture argl of counter is
    signal err_cs : std_logic := '0';
    signal err_ns : std_logic;
    signal cnt_cs:  std_logic_vector(11 downto 0) := (others => '0');
    signal cnt_ns:  std_logic_vector(11 downto 0);
	signal carryBitChain_s : std_logic_vector(0 to 12) := (others => '0');
    signal one:  std_logic_vector(11 downto 0);
    signal up_cs: std_logic := '0';
    signal down_cs: std_logic := '0';
    signal carryOut: std_logic;
    component fa is
        port (
        result : out std_logic;
        co : out std_logic;
        --ins
        up : in std_logic;
        down: in std_logic;
        a : in std_logic; 
        b : in std_logic; 
        ci : in std_logic
        );
    end component fa;
    for all : fa use entity work.fa( beh );
    
begin
    one <= "000000000001";
    did <= "100";
    carryBitChain_s(0) <= '0';
	fa_gen:
    for I in 0 to 11 generate
        fa_i : fa port map (result=>cnt_ns(I),  co=>carryBitChain_s(I+1), ci=>carryBitChain_s(I), a=>cnt_cs(I), b=>one(I), up =>up_cs, down => down_cs);
    end generate fa_gen;
    carryOut <= carryBitChain_s(12);
    

    err_check:
    process(up_cs, down_cs, carryOut,err_cs) is
		variable err_v : std_logic := '0';
		variable op_check : std_logic := '0';
    begin
		op_check:= down_cs xor up_cs;
		err_v:=err_cs;
		if op_check = '1' then
		   err_ns <= ((up_cs and carryOut)xor(not carryOut and down_cs)) or err_v;
		else
		   err_ns <= err_v;
		end if;
    end process err_check;
    
    regCheck:
    process ( clk ) is
    begin
		if clk = '1' and clk'event then
			if nres = '0' then
				err_cs <= '0';
				cnt_cs <= (others => '0');
				up_cs <= '0';
				down_cs <= '0';
			else
				err_cs <= err_ns;
				if nLd = '0' then
				  cnt_cs <= valLd;
				else
				   cnt_cs <= cnt_ns;
				end if;
				up_cs <= up;
				down_cs <= down;
			end if;    
		end if;
    end process regCheck;
    
    err <= err_cs;
    cnt <= cnt_cs;
end architecture argl;