--@authors: Mir Farshid Baha,2141801, mirfarshid.baha@haw-hamburg.de
--          Mehmet Cakir, 2195657, mehmet.cakir@haw-hamburg.de

--fullAdder for bitwise in- and decrementing

library work;
    use work.all;
    
library ieee;
    use ieee.std_logic_1164.all;
    use ieee.std_logic_arith.all;
    use ieee.std_logic_unsigned.all;
    
entity fa is
    port (
        --outs
        result : out std_logic;
        co : out std_logic;
        --ins
        up : in std_logic;
        down: in std_logic;
        a : in std_logic; -- 
        b : in std_logic; -- only relevant for add
        ci : in std_logic
    );
end entity fa;

architecture beh of fa is
begin
calculate:
    process(up,down,ci,a,b)
		variable op_sync: std_logic;
		variable up_check: std_logic;
    begin
        op_sync := up xor down;--makes sure only up or down are active at the time
        up_check := op_sync and up;--determines the operation
        if op_sync = '1' then
            if up_check = '1' then
               co <= (a and b) or (a and ci) or (b and ci);
               result <= a xor b xor ci;
            else
               co <= (a and op_sync) or (a and ci) or (op_sync and ci);
               result <= a xor op_sync xor ci;
            end if;
        else
            result <= a;
            co <= ci;
        end if;    
    end process calculate;
end architecture beh;