onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -divider -height 50 INPUTS
add wave -noupdate /tbdut/valLd_s
add wave -noupdate -divider -height 20 <NULL>
add wave -noupdate /tbdut/nLd_s
add wave -noupdate -divider -height 20 <NULL>
add wave -noupdate /tbdut/up_s
add wave -noupdate -divider -height 20 <NULL>
add wave -noupdate /tbdut/down_s
add wave -noupdate -divider -height 20 <NULL>
add wave -noupdate /tbdut/clk_s
add wave -noupdate -divider -height 20 <NULL>
add wave -noupdate /tbdut/nres_s
add wave -noupdate -divider -height 50 OUTPUTS
add wave -noupdate /tbdut/err_s
add wave -noupdate -divider -height 20 <NULL>
add wave -noupdate /tbdut/cnt_s
add wave -noupdate -divider -height 20 <NULL>
add wave -noupdate /tbdut/did_s
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {1650 ns} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {1650 ns} {3851 ns}
