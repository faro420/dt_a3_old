--@authors: Mir Farshid Baha,2141801, mirfarshid.baha@haw-hamburg.de
--          Mehmet Cakir, 2195657, mehmet.cakir@haw-hamburg.de

-- test bench for the entity counter
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

library work;
use work.all;

entity tb_rc_dut is
end tb_rc_dut;

architecture beh of tb_rc_dut is
    signal valLd_s : std_logic_vector(11 downto 0);
    signal nLd_s : std_logic;
    signal up_s : std_logic;
    signal down_s : std_logic;
    signal clk_s : std_logic;
    signal nres_s : std_logic;
    signal did_s : std_logic_vector(2 downto 0);
    signal cnt_s1 : std_logic_vector(11 downto 0);
    signal cnt_s2 : std_logic_vector(11 downto 0);
    signal cnt_s3 : std_logic_vector(11 downto 0);
    signal cnt_s4 : std_logic_vector(11 downto 0);
    signal cnt_s5 : std_logic_vector(11 downto 0);
    signal cnt_s6 : std_logic_vector(11 downto 0);
    signal err_s : std_logic;
    signal checkOk_s : boolean;
    signal equal_s   : std_logic;

    component sg is
        port (
            valLd : out std_logic_vector( 11 downto 0);
            nLd : out std_logic;
            up : out std_logic;
            down : out std_logic;
            clk  : out std_logic;
            nres : out std_logic
        );
    end component sg;
    for all : sg use entity work.sg( beh );

    component dut is
	    port(
            --outputs
            did : out std_logic_vector(2 downto 0);
            err : out std_logic;
            cnt : out std_logic_vector(11 downto 0);
            --inputs
            valLd: in std_logic_vector(11 downto 0);
            nLd: in std_logic;
            up:	 in std_logic;		
            down: in std_logic;
            --clock and reset
            clk : in std_logic;
            nres: in std_logic 
        );
    end component;
    -- for all: dut use entity work.dut ( atgl );
    
    component dut_rc is
        port(
            checkOk : out   boolean;            -- all OK resp. NO falut detected
            equal   : out   std_logic;          -- all results are equal/the same (at end of clk cycle)

            aril_res : in std_logic_vector(11 downto 0);   -- result 
            argl_res : in std_logic_vector(11 downto 0);   -- result 
            arop_res : in std_logic_vector(11 downto 0);   -- result 
            atil_res : in std_logic_vector(11 downto 0);   -- result 
            atgl_res : in std_logic_vector(11 downto 0);   -- result 
            atop_res : in std_logic_vector(11 downto 0);   -- result 
            clk     : in std_logic;          -- clock
            nres    : in std_logic           -- low active synchronous RESet
        );
    end component;
    for all: dut_rc use entity work.dut_rc (beh);
    
    
begin
	sg_i : sg
        port map (
            valLd => valLd_s,
            nLd => nLd_s,
            up => up_s,
            down => down_s,
            clk => clk_s,
            nres => nres_s
        );
    
    dut_i1 : entity work.dut(aril_w)
        port map (
            valLd => valLd_s,
            nLd => nLd_s,
            up => up_s,
            down => down_s,
            clk => clk_s,
            nres => nres_s,
            did => did_s,
            err => err_s,
            cnt => cnt_s1
        );
    dut_i2 : entity work.dut(arop_w)
        port map (
            valLd => valLd_s,
            nLd => nLd_s,
            up => up_s,
            down => down_s,
            clk => clk_s,
            nres => nres_s,
            did => did_s,
            err => err_s,
            cnt => cnt_s2
        );
    dut_i3 : entity work.dut(argl_w)
        port map (
            valLd => valLd_s,
            nLd => nLd_s,
            up => up_s,
            down => down_s,
            clk => clk_s,
            nres => nres_s,
            did => did_s,
            err => err_s,
            cnt => cnt_s3
        );
    dut_i4 : entity work.dut(atil)
        port map (
            valLd => valLd_s,
            nLd => nLd_s,
            up => up_s,
            down => down_s,
            clk => clk_s,
            nres => nres_s,
            did => did_s,
            err => err_s,
            cnt => cnt_s4
        );
    dut_i5 : entity work.dut(atop)
        port map (
            valLd => valLd_s,
            nLd => nLd_s,
            up => up_s,
            down => down_s,
            clk => clk_s,
            nres => nres_s,
            did => did_s,
            err => err_s,
            cnt => cnt_s5
        );
    dut_i6 : entity work.dut(atgl)
        port map (
            valLd => valLd_s,
            nLd => nLd_s,
            up => up_s,
            down => down_s,
            clk => clk_s,
            nres => nres_s,
            did => did_s,
            err => err_s,
            cnt => cnt_s6
        );
     
    dut_rc_i : dut_rc
        port map (
            checkOk => checkOk_s,
            equal => equal_s,

            aril_res => cnt_s1,
            arop_res => cnt_s2,
            argl_res => cnt_s3,
            atil_res => cnt_s4,
            atop_res => cnt_s5,
            atgl_res => cnt_s6,
            clk => clk_s,
            nres => nres_s
        );
end beh;