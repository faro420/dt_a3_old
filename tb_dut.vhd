--@authors: Mir Farshid Baha,2141801, mirfarshid.baha@haw-hamburg.de
--          Mehmet Cakir, 2195657, mehmet.cakir@haw-hamburg.de

-- test bench for the entity counter
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

library work;
use work.all;

entity tbDut is
end tbDut;

architecture beh of tbDut is
    signal valLd_s : std_logic_vector(11 downto 0);
    signal nLd_s : std_logic;
    signal up_s : std_logic;
    signal down_s : std_logic;
    signal clk_s : std_logic;
    signal nres_s : std_logic;
    signal did_s : std_logic_vector(2 downto 0);
    signal cnt_s : std_logic_vector(11 downto 0);
    signal err_s : std_logic;

    component sg is
        port (
            valLd : out std_logic_vector( 11 downto 0);
            nLd : out std_logic;
            up : out std_logic;
            down : out std_logic;
            clk  : out std_logic;
            nres : out std_logic
        );
    end component sg;
    for all : sg use entity work.sg( beh );

    component dut is
	    port(
            --outputs
            did : out std_logic_vector(2 downto 0);
            err : out std_logic;
            cnt : out std_logic_vector(11 downto 0);
            --inputs
            valLd: in std_logic_vector(11 downto 0);
            nLd: in std_logic;
            up:	 in std_logic;		
            down: in std_logic;
            --clock and reset
            clk : in std_logic;
            nres: in std_logic 
        );
    end component;
    -- for all: dut use entity work.dut ( argl );

begin
	sg_i : sg
        port map (
            valLd => valLd_s,
            nLd => nLd_s,
            up => up_s,
            down => down_s,
            clk => clk_s,
            nres => nres_s
        );
    
    -- dut_i : dut
    dut_i : entity work.dut(aril_w)
        port map (
            valLd => valLd_s,
            nLd => nLd_s,
            up => up_s,
            down => down_s,
            clk => clk_s,
            nres => nres_s,
            did => did_s,
            err => err_s,
            cnt => cnt_s
        );
end beh;