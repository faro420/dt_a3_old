library work;
    use work.all;
    
library ieee;
    use ieee.std_logic_1164.all;
    
    
    
    
    
entity dut_rc is
    port (
        checkOk : out   boolean;            -- all OK resp. NO falut detected
        equal   : out   std_logic;          -- all results are equal/the same (at end of clk cycle)

        aril_res : in std_logic_vector(11 downto 0);   -- result 
        argl_res : in std_logic_vector(11 downto 0);   -- result 
        arop_res : in std_logic_vector(11 downto 0);   -- result 
        atil_res : in std_logic_vector(11 downto 0);   -- result 
        atgl_res : in std_logic_vector(11 downto 0);   -- result 
        atop_res : in std_logic_vector(11 downto 0);   -- result 
        clk     : in std_logic;          -- clock
        nres    : in std_logic           -- low active synchronous RESet
    );--}port
end entity dut_rc;


architecture beh of dut_rc is

begin
    
    responseChecker:
    process is
        variable equal_v : std_logic;                   -- temporary and finally final result
    begin
        checkOk <= true;                                -- no fault detected yet
        equal   <= 'H';                                 -- no difference yet  and until reset it's tolerable
        
        if nres/='0' then wait until nres='0'; end if;  -- assure that reset is active
        -- after reset is activated all implementation shall behave the same way
        -- as result of timing differences it is checked at THE END of the clk cycle
        -- since signals need at least one delta cycle for transportation,
        -- this check itself is done on rising edge of new clk cycle
        
        loop
            wait until  '1'=clk and clk'event;
            
            equal_v := '1';                             -- up to now all is equal
            checkLoop:
            for I in aril_res'low to aril_res'high loop
                if  aril_res(I)/=argl_res(I)  then
                    equal_v := '0';                     -- mark difference
                    exit checkLoop;                     -- no more tests required
                end if;
                
                if  argl_res(I)/=arop_res(I)  then
                    equal_v := '0';                     -- mark difference
                    exit checkLoop;                     -- no more tests required
                end if;
                
                if  arop_res(I)/=atil_res(I)  then
                    equal_v := '0';                     -- mark difference
                    exit checkLoop;                     -- no more tests required
                end if;
                
                if  atil_res(I)/=atgl_res(I)  then
                    equal_v := '0';                     -- mark difference
                    exit checkLoop;                     -- no more tests required
                end if;
                
                if  atgl_res(I)/=atop_res(I)  then
                    equal_v := '0';                     -- mark difference
                    exit checkLoop;                     -- no more tests required
                end if;
            end loop;
            
            equal <= equal_v;
            if  '1'/=equal_v  then  checkOk <= false;  end if;
        end loop;
        
        wait;
    end process responseChecker;        
    
end architecture beh;
