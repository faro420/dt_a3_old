Autoren:    Mir Farshid Baha,2141801, mirfarshid.baha@haw-hamburg.de
            Mehmet Cakir, 2195657, mehmet.cakir@haw-hamburg.de
            
Aufgabe 3) Aufgabenstellung und Beschreibung befindet sich in der PDF-Datei.
Es geht um die Realisierung eines Zaehlers in VHDL (Counter) auf 3 verschiedene Art
und Weisen:
1. Durch ueberladene Operatoren "+" und "-"
2. Mit Hilfe von eine iterativen Loop und booleschen Variablen
3. Struktur, die auf 1Bit-Komponenten fuer das In-/Dekrementieren zurueckgreift und diese
mit einem "Generate"-Loop instanziert