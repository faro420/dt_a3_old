--@authors: Mir Farshid Baha,2141801, mirfarshid.baha@haw-hamburg.de
--          Mehmet Cakir, 2195657, mehmet.cakir@haw-hamburg.de

architecture arop of counter is
    signal err_cs : std_logic := '0';
    signal err_ns : std_logic;
    signal cnt_cs:  std_logic_vector(11 downto 0) := (others => '0');
    signal cnt_ns:  std_logic_vector(11 downto 0);
    signal up_cs: std_logic := '0';
    signal down_cs: std_logic := '0';

begin
    did <= "001";
    process(up_cs, down_cs) is
    --variables belong here
		variable upperLimit_v : std_logic_vector(11 downto 0);
		variable lowerLimit_v : std_logic_vector(11 downto 0);
		variable addSubVector_v : std_logic_vector(11 downto 0);
		variable cnt_v : std_logic_vector(11 downto 0);
		variable up_v : std_logic ;
		variable down_v : std_logic;
		variable err_v : std_logic;
    begin
    --procedure belongs here
		err_v:= err_cs;
		cnt_v := cnt_cs;
		up_v := up_cs;
		down_v := down_cs;
		addSubVector_v(0) := '1'; 
		if  up_v = '1' and down_v = '0' then
			if cnt_v = upperLimit_v then
				err_v:= '1';
			else
				err_v:= err_cs;
			end if;
			cnt_v := cnt_v + addSubVector_v;
		elsif  down_v = '1' and up_v = '0' then
			if cnt_v = lowerLimit_v then
			   err_v := '1';
			else
			   err_v:= err_cs;
			end if;
			cnt_v := cnt_v - addSubVector_v;		
		else
			err_v:= err_cs;
		end if;
			err_ns <= err_v;	
			cnt_ns <= cnt_v;
    end process;
    
    regCheck:
    process ( clk ) is
    begin
		if clk = '1' and clk'event then
			if nres = '0' then
				err_cs <= '0';
				cnt_cs <= (others => '0');
				up_cs <= '0';		
				down_cs <= '0';
			else
				err_cs <= err_ns;
				if nLd = '0' then
				   cnt_cs <= valLd;
				else
				   cnt_cs <= cnt_ns;
				end if;
				up_cs <= up;
				down_cs <= down;
			end if;    
		end if;
    end process regCheck;
    
    err <= err_cs;
    cnt <= cnt_cs;
end architecture arop;