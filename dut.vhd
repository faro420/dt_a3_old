--@authors: Mir Farshid Baha,2141801, mirfarshid.baha@haw-hamburg.de
--          Mehmet Cakir, 2195657, mehmet.cakir@haw-hamburg.de

-- wrapper for counter
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity dut is
    port(
        --outputs
        did : out std_logic_vector(2 downto 0);
        err : out std_logic;
        cnt : out std_logic_vector(11 downto 0);
        --inputs
        valLd: in std_logic_vector(11 downto 0);
        nLd: in std_logic;
        up:	 in std_logic;		
        down: in std_logic;
        --clock and reset
        clk : in std_logic;
        nres: in std_logic 
    );
end entity dut;

architecture rtl of dut is
    component counter is
    port(
        --outputs
        did : out std_logic_vector(2 downto 0);
        err : out std_logic;
        cnt : out std_logic_vector(11 downto 0);
        --inputs
        valLd: in std_logic_vector(11 downto 0);
        nLd: in std_logic;
        up: in std_logic;
        down: in std_logic;
        --clock and reset
        clk : in std_logic;
        nres: in std_logic 
    );
    end component counter;
    for all : counter use entity work.counter(argl);
begin
    counter_c : counter
        port map (
            --outputs
            did => did,
            err => err, 
            cnt => cnt,
            --inputs
            valLd => valLd,
            nLd => nLd,
            up => up,
            down => down,
            --clock and reset
            clk => clk,
            nres => nres 
        );
end architecture rtl;
------------------------------------------------------------
-- -----------------------------------------------------------------
-- architecture argl_w of dut is
    -- component counter is
    -- port(
        -- --outputs
        -- did : out std_logic_vector(2 downto 0);
        -- err : out std_logic;
        -- cnt : out std_logic_vector(11 downto 0);
        -- --inputs
        -- valLd: in std_logic_vector(11 downto 0);
        -- nLd: in std_logic;
        -- up: in std_logic;
        -- down: in std_logic;
        -- --clock and reset
        -- clk : in std_logic;
        -- nres: in std_logic 
    -- );
    -- end component counter;
    -- for all : counter use entity work.counter(argl);
-- begin
    -- counter_c : counter
        -- port map (
            -- --outputs
            -- did => did,
            -- err => err, 
            -- cnt => cnt,
            -- --inputs
            -- valLd => valLd,
            -- nLd => nLd,
            -- up => up,
            -- down => down,
            -- --clock and reset
            -- clk => clk,
            -- nres => nres 
        -- );
-- end argl_w;
-- ------------------------------------------------------------
-- ------------------------------------------------------------
-- architecture aril_w of dut is
    -- component counter is
    -- port(
        -- --outputs
        -- did : out std_logic_vector(2 downto 0);
        -- err : out std_logic;
        -- cnt : out std_logic_vector(11 downto 0);
        -- --inputs
        -- valLd: in std_logic_vector(11 downto 0);
        -- nLd: in std_logic;
        -- up: in std_logic;
        -- down: in std_logic;
        -- --clock and reset
        -- clk : in std_logic;
        -- nres: in std_logic 
    -- );
    -- end component counter;
    -- for all : counter use entity work.counter(aril);
-- begin
    -- counter_c : counter
        -- port map (
            -- --outputs
            -- did => did,
            -- err => err, 
            -- cnt => cnt,
            -- --inputs
            -- valLd => valLd,
            -- nLd => nLd,
            -- up => up,
            -- down => down,
            -- --clock and reset
            -- clk => clk,
            -- nres => nres 
        -- );
-- end aril_w;