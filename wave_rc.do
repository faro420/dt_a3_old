onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -divider -height 50 SG
add wave -noupdate /tb_rc_dut/valLd_s
add wave -noupdate -divider -height 20 <NULL>
add wave -noupdate /tb_rc_dut/nLd_s
add wave -noupdate -divider -height 20 <NULL>
add wave -noupdate /tb_rc_dut/up_s
add wave -noupdate -divider -height 20 <NULL>
add wave -noupdate /tb_rc_dut/down_s
add wave -noupdate -divider -height 20 <NULL>
add wave -noupdate /tb_rc_dut/clk_s
add wave -noupdate -divider -height 20 <NULL>
add wave -noupdate /tb_rc_dut/nres_s
add wave -noupdate -divider -height 50 RESULTS
add wave -noupdate -divider -height 30 FUNCTIONAL
add wave -noupdate /tb_rc_dut/cnt_s1
add wave -noupdate -divider -height 20 <NULL>
add wave -noupdate /tb_rc_dut/cnt_s2
add wave -noupdate -divider -height 20 <NULL>
add wave -noupdate /tb_rc_dut/cnt_s3
add wave -noupdate -divider -height 30 TIMING
add wave -noupdate /tb_rc_dut/cnt_s4
add wave -noupdate -divider -height 20 <NULL>
add wave -noupdate /tb_rc_dut/cnt_s5
add wave -noupdate -divider -height 20 <NULL>
add wave -noupdate /tb_rc_dut/cnt_s6
add wave -noupdate -divider -height 50 RC-OUTPUTS
add wave -noupdate /tb_rc_dut/err_s
add wave -noupdate -divider -height 20 <NULL>
add wave -noupdate /tb_rc_dut/checkOk_s
add wave -noupdate -divider -height 20 <NULL>
add wave -noupdate /tb_rc_dut/equal_s
add wave -noupdate -divider -height 20 <NULL>
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {14654867 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {31500 ns}
