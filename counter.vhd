--@authors: Mir Farshid Baha,2141801, mirfarshid.baha@haw-hamburg.de
--          Mehmet Cakir, 2195657, mehmet.cakir@haw-hamburg.de

-- Abstraction for the 3 different counter devices
library work;
    use work.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

entity counter is
    port(
        --outputs
        did : out std_logic_vector(2 downto 0);
        err : out std_logic;
        cnt : out std_logic_vector(11 downto 0);
        --inputs
        valLd: in std_logic_vector(11 downto 0);
        nLd: in std_logic;
        up: in std_logic;
        down: in std_logic;
        --clock and reset
        clk : in std_logic;
        nres: in std_logic 
    );
end entity counter;
